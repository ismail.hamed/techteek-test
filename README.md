# Teachteek Test - News CMS
build a polished MVP of a news CMS where clients can create articles with GIF images.
## Table of contents

- [Overview](#overview)
    - [Features](#features)
    - [Screenshot](#screenshot)
    - [Links](#links)
- [My process](#my-process)
    - [Built with](#built-with)
- [Project setup](#project-setup)
## Overview

### Features

Users should be able to:
-  Login and Log out and Register (Laravel)
-  Create new article (by logged-in users).
-  Explore articles (publicly accessible).
-  GIF images search plugin
-  Get GIF images from API ( Node js)
-  **Bonus** Copy URL from provider and paste it to the editor, it should show the GIF image
   instead of the URL (Bonus).

### Screenshot

![](./screenshot-1.png)
![](./screenshot-2.png)
![](./screenshot-3.png)
### Links

[Solution ](https://gitlab.com/ismail.hamed/techteek-test)


## My process

### Built with

- [Laravel](https://laravel.com/)
- [Nodejs](https://nodejs.org/)
- [Vue.js](https://vuejs.org/)

## Project setup
### Laravel API with consumer: ##
 -  Open the console and cd laravel-api-with-consumer directory
 -  Rename .env.example file to .env inside your project root and fill the database information.
 -  Create new Database
 -  Run composer install or php composer.phar install
 -  Run php artisan key:generate
 -  Run php artisan migrate
 -  Run php artisan db:seed
 -  Run npm install
 -  Run npm run dev
 -  Run php artisan serve

You can now access your project at localhost:8080

### Gif search API: ##


 -  Open the console and cd gif-search-api directory
-  Rename .env.example file to .env inside your project root .
 -  You have to register to giphy as a developer to get app key
 -  Set app key to GIPHY_APP_KEY
 -  npm install
 -  node app.js

You can now access your project at localhost:3000
