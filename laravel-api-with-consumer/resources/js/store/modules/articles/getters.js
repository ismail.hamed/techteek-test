export default {
    articles(state) {
        return state.articles;
    },
    hasArticles(state) {
        return state.articles && state.articles.length > 0;
    },
    article(state) {
        return state.article;
    },
};
