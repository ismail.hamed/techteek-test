import axios from 'axios';

export default {
    async loadArticles(context) {
        const response = await axios.get(`/api/articles`);
        const responseData = await response.data;
        if (!response.status === 200) {
            const error = new Error(responseData.message || 'Failed to fetch!');
            throw error;
        }

        const articles = responseData.data;
        context.commit('setArticles', articles);
    },
    async loadArticle(context, slug) {
        const response = await axios.get(`/api/articles/${slug}`);
        const responseData = await response.data;
        if (!response.status === 200) {
            const error = new Error(responseData.message || 'Failed to fetch!');
            throw error;
        }
        const article = responseData.data;
        context.commit('setArticle', article);
    },

    async crateArticle(context, data) {
        const articleData = {
            title: data.title,
            content:data.content,
        };
        const response = await axios.post('/api/articles', articleData);
        const responseData = await response;
        if (!response.status === 200) {
            const error = new Error(
                responseData.message || 'Failed to authenticate. Check your login data.'
            );
            throw error;
        }
        // context.commit('registerCoach', {
        //     ...coachData,
        //     id: userId
        // });

        return responseData;

    },
};
