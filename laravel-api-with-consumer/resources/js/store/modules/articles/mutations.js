export default {
    setArticles(state, payload) {
        state.articles = payload;
    },
    setArticle(state, payload) {
        state.article = payload;
    },
};
