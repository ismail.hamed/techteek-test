import store from '../index'
import axios from 'axios';

const BASE_URL = import.meta.env.VITE_APP_BASEURL
const instance = axios.create({
    baseURL: BASE_URL,
    withCredentials: true
});

instance.interceptors.request.use(
    config => {
        if (store.getters.token) {
            config.headers['Authorization'] = `Bearer ${store.getters.token}`;
        }
        config.headers['Content-Type'] = 'application/json';
        config.headers['Accept'] = 'application/json';
        return config;
    },
    error => {
        Promise.reject(error);
    }
);
export default instance;
