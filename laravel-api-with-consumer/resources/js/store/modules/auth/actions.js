let timer;
import axios from 'axios';

export default {
    async login(context, payload) {
        await axios.get('/sanctum/csrf-cookie')
        const response = await axios.post('/api/auth/login', payload);
        const responseData = response.data.data;
        if (!response.status === 200) {
            const error = new Error(
                responseData.message || 'Failed to authenticate. Check your login data.'
            );
            throw error;
        }

        const expiresIn = +responseData.expires_in * 60000;
        // const expiresIn = 5000;
        const expirationDate = new Date().getTime() + expiresIn;
        localStorage.setItem('token', responseData.access_token);
        localStorage.setItem('userId', responseData.me.id);
        localStorage.setItem('tokenExpiration', expirationDate);

        timer = setTimeout(function () {
            context.dispatch('autoLogout');
        }, expiresIn);

        context.commit('setUser', {
            token: responseData.access_token,
            userId: responseData.me.id
        });
    },
    async register(context, payload) {
        await axios.get('/sanctum/csrf-cookie')
        const response = await axios.post('/api/auth/register', payload);
        const responseData = response.data.data;
        if (!response.status === 200) {
            const error = new Error(
                responseData.message || 'Failed to authenticate. Check your login data.'
            );
            throw error;
        }
    },

    tryLogin(context) {
        const token = localStorage.getItem('token');
        const userId = localStorage.getItem('userId');
        const tokenExpiration = localStorage.getItem('tokenExpiration');

        const expiresIn = +tokenExpiration - new Date().getTime();

        if (expiresIn < 0) {
            return;
        }

        timer = setTimeout(function () {
            context.dispatch('autoLogout');
        }, expiresIn);

        if (token && userId) {
            context.commit('setUser', {
                token: token,
                userId: userId
            });
        }
    },
    logout(context) {
        localStorage.removeItem('token');
        localStorage.removeItem('userId');
        localStorage.removeItem('tokenExpiration');

        clearTimeout(timer);

        context.commit('setUser', {
            token: null,
            userId: null
        });
    },
    autoLogout(context) {
        context.dispatch('logout');
        context.commit('setAutoLogout');
    }
};
