import {defineAsyncComponent} from 'vue';
import {createRouter, createWebHistory} from 'vue-router';

import NotFound from './pages/NotFound.vue';
import store from './store/index.js';
import ArticlesList from "./pages/articles/ArticlesList.vue";

const ArticleDetail = defineAsyncComponent(() =>
    import('./pages/articles/ArticleDetail.vue')
);
const ArticleCreate = defineAsyncComponent(() =>
    import('./pages/articles/ArticleCreate.vue')
);

const Login = defineAsyncComponent(() =>
    import('./pages/auth/Login.vue')
);
const Register = defineAsyncComponent(() =>
    import('./pages/auth/Register.vue')
);
const router = createRouter({
    history: createWebHistory(),
    routes: [
        {path: '/', redirect: '/articles'},
        {path: '/articles', component: ArticlesList},
        {
            path: '/articles/:slug',
            component: ArticleDetail,
            props: true,
        },
        {
            path: '/articles/create',
            component: ArticleCreate,
            meta: {requiresAuth: true}
        },
        {path: '/auth/login', component: Login, meta: {requiresUnauth: true}},
        {path: '/auth/register', component: Register, meta: {requiresUnauth: true}},

        {path: '/:notFound(.*)', component: NotFound}
    ]
});

router.beforeEach(function (to, _, next) {
    if (to.meta.requiresAuth && !store.getters.isAuthenticated) {
        next('/auth');
    } else if (to.meta.requiresUnauth && store.getters.isAuthenticated) {
        next('/articles');
    } else {
        next();
    }
});

export default router;
