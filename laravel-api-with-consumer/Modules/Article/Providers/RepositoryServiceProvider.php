<?php

namespace Modules\Article\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Article\Contracts\ArticleInterface;
use Modules\Article\Repositories\ArticleRepository;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $toBind = [
            ArticleInterface::class => ArticleRepository::class,
        ];

        foreach ($toBind as $interface => $implementation) {
            $this->app->bind($interface, $implementation);
        }
//        $this->app->bind(CategoryInterface::class, function ($app) {
//            return new CachingCategoryRepository(new CategoryRepository(new Category));
//        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
