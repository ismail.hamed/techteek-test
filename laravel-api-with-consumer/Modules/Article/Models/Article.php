<?php

namespace Modules\Article\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Modules\Media\Models\Media;
use Modules\User\Database\factories\ArticleFactory;
use Modules\User\Models\User;

class Article extends Model
{
    use HasFactory;

    protected $fillable =
        [
            'title',
            'content',
            'slug'
        ];

    protected $guarded =
        [
            'user_id',
        ];
    protected $casts = [
        'content' => 'array'
    ];

    protected static function newFactory()
    {
        return ArticleFactory::new();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function medias()
    {
        return $this->morphMany(Media::class, 'mediaable');
    }
}
