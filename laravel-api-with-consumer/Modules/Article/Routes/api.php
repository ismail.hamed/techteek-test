<?php

use Modules\Article\Http\Controllers\ArticleController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware(['auth:sanctum'])->group(function () {
    Route::post('/articles',[ ArticleController::class, 'store']);
});
Route::get('/articles/{article:slug}', [ArticleController::class, 'show']);
Route::get('/articles', [ArticleController::class, 'index']);
