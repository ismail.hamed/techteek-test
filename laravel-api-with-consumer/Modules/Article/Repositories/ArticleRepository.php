<?php

namespace Modules\Article\Repositories;

use Modules\Article\Contracts\ArticleInterface;
use Modules\Article\Models\Article;
use Modules\Core\Repositories\BaseRepository;

class ArticleRepository extends BaseRepository implements ArticleInterface
{

    public function __construct(Article $model)
    {
        parent::__construct($model); // Inject the model that you need to build queries from
    }

}
