<?php

namespace Modules\Article\Services;

use Illuminate\Support\Facades\Auth;
use Modules\Article\Contracts\ArticleInterface;
use Modules\Article\Models\Article;
use Modules\Core\Services\BaseService;

class ArticleService extends BaseService
{
    protected $article;

    public function __construct(ArticleInterface $article)
    {
        $this->article = $article;
    }


    public function getArticles()
    {
        $per_page = request('per_page', 25);
        $criteria = [];
        $columns = ['id', 'title', 'user_id', 'slug', 'created_at'];
        $relations = [];
        $articles = $this->article->getByCriteriaAndPerPage($criteria, $columns, $relations, $per_page);
        return $this->successResponse($articles);
    }

    public function addNewArticle(array $data)
    {
        $article = new Article($data);
        $article->user_id = Auth::user()->id;
        $article->save();
        $message = trans('article::messages.add_successfully');
        return $this->successResponse(null, $message);
    }
}
