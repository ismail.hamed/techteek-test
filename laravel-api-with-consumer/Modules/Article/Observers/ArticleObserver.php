<?php

namespace Modules\Article\Observers;


use Illuminate\Support\Str;
use Modules\Article\Models\Article;

class ArticleObserver
{
    public function created(Article $article)
    {
        $slug = Str::slug($article->title, '-');
        $slug = $slug . '-' . $article->id;
        $article->update(['slug' => $slug]);
    }
}
