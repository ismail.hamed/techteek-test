<?php


namespace Modules\Article\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;


class StoreArticleRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'title' => ['required', 'max:255'],
            'content' => ['required', 'array'],
        ];
    }
}
