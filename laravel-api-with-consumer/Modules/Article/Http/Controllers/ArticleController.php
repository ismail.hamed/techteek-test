<?php

namespace Modules\Article\Http\Controllers;

use Modules\Article\Http\Requests\StoreArticleRequest;
use Modules\Article\Models\Article;
use Modules\Article\Services\ArticleService;
use Modules\Article\Transformers\ArticleResource;
use Modules\Core\Http\Controllers\API\ApiBaseController;

class ArticleController extends ApiBaseController
{
    protected $articleService;

    public function __construct(ArticleService $articleService)
    {
        $this->articleService = $articleService;
    }


    public function index()
    {
        $result = $this->articleService->getArticles();
        if ($result['result']) {
            $result['data'] = ArticleResource::collection($result['data']);
        }
        return $this->apiResponse($result);
    }

    public function store(StoreArticleRequest $request)
    {
        $result = $this->articleService->addNewArticle($request->all());
        return $this->apiResponse($result);
    }

    public function show(Article $article)
    {
        $result['data'] = new ArticleResource($article);
        return $this->successResponse($result);
    }

}
