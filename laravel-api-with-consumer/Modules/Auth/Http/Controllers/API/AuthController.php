<?php

namespace Modules\Auth\Http\Controllers\API;

use Modules\Auth\Http\Requests\ChangePasswordRequest;
use Modules\Auth\Http\Requests\LoginRequest;
use Modules\Auth\Http\Requests\RegisterRequest;
use Modules\Auth\Services\AuthService;
use Modules\Core\Http\Controllers\API\ApiBaseController;
use Modules\User\Transformers\UserResource;


class AuthController extends ApiBaseController
{
    protected $authService;

    public function __construct(AuthService $authService)
    {
        $this->authService = $authService;
    }


    public function login(LoginRequest $request)
    {
        $credentials = request(['email', 'password']);
        $result = $this->authService->login($credentials);
        if ($result['result']) {
            $data = &$result['data'];
            $data['me'] = new UserResource($data['me']);
        }
        return $this->apiResponse($result);
    }


    public function register(RegisterRequest $request)
    {
        $result = $this->authService->register($request->validated());
        return $this->apiResponse($result);
    }

    public function me()
    {
        $result = $this->authService->me();
        if ($result['result']) {
            $result['data'] = new UserResource($result['data']);
        }
        return $this->apiResponse($result);
    }

    public function logout()
    {
        $result = $this->authService->logout();
        return $this->apiResponse($result);
    }

    public function changePassword(ChangePasswordRequest $request)
    {
        $result = $this->authService->changePassword($request->validated());
        return $this->apiResponse($result);
    }

}
