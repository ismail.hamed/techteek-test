<?php

namespace Modules\Auth\Services;

use Illuminate\Support\Facades\Auth;
use Modules\Core\Services\BaseService;
use Modules\User\Contracts\UserInterface;
use Symfony\Component\HttpFoundation\Response;

class AuthService extends BaseService
{
    protected $user;

    public function __construct(UserInterface $user)
    {
        $this->user = $user;
    }

    public function login($credentials)
    {
        if (!Auth::attempt($credentials)) {
            $message = trans('auth::messages.username_or_password_not_correct');
            return $this->errorResponse($message, Response::HTTP_UNAUTHORIZED);
        } else {
            $token = $this->respondWithToken();
            return $this->successResponse($token);
        }
    }

    protected function respondWithToken()
    {
        $token = Auth::user()->createToken('auth_token')->plainTextToken;
        return [
            'access_token' => $token,
            'token_type' => 'Bearer',
            'expires_in' => config('sanctum.expiration'),
            'me' => Auth::user(),
        ];
    }

    public function register($data)
    {
        $this->user->create($data);
        $message = trans('auth::messages.register_successfully');
        return $this->successResponse(null, $message);
    }

    public function logout()
    {
        Auth::user()->tokens()->delete();
        $message = trans('auth::messages.log_out_successfully');
        return $this->successResponse(null, $message);
    }

    public function me()
    {
        return $this->successResponse(Auth::user());
    }

    public function changePassword($data)
    {
        $is_updated = Auth::user()->fill(['password' => $data['new_password']])->save();
        if ($is_updated) {
            $message = trans('auth::messages.user_password_updated');
            return $this->successResponse(null, $message);
        } else {
            $message = trans('auth::messages.something_went_wrong');
            return $this->errorResponse(null, $message);
        }
    }
}
