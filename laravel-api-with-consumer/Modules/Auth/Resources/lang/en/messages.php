<?php

return [
    'username_or_password_not_correct' => "username or password isn't correct",
    'log_out_successfully' => "Successfully logged out",
    'register_successfully' => "Register successfully",
    'user_password_updated' => "User's password updated successfully.",
    'something_went_wrong' => 'Something Went Wrong',
    'sent' => 'We have emailed your password reset link!',
    'reset' => 'Your password has been reset!',
    'invalid' => 'Your code is invalid!',
];
