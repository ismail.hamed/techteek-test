<?php

namespace Modules\Auth\Tests\Feature;

use Modules\Core\Tests\Feature\ApiTestCase;
use Modules\User\Models\User;
use Symfony\Component\HttpFoundation\Response;

class RegisterTest extends ApiTestCase
{
    public function test_email_is_required()
    {

        $response = $this->postJson('auth/register', array_merge($this->request_data(), ['email' => '']));
        $response
            ->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJson([
                'message' => 'The email field is required.',
                'errors' => [
                    "email" => ['The email field is required.'],
                ],
            ]);
    }

    private function request_data()
    {
        $password = $this->faker->password;
        return [
            'name' => $this->faker->name(),
            'email' => $this->faker->email,
            'password' => $password,
            'password_confirmation' => $password,
        ];
    }

    public function test_name_is_required()
    {

        $response = $this->postJson('auth/register', array_merge($this->request_data(), ['name' => '']));
        $response
            ->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJson([
                'message' => 'The name field is required.',
                'errors' => [
                    "name" => ['The name field is required.'],
                ],
            ]);
    }

    public function test_name_maximize()
    {

        $response = $this->postJson('auth/register', array_merge($this->request_data(), ['name' => $this->faker->text(500)]));
        $response
            ->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJson([
                'message' => 'The name must not be greater than 255 characters.',
                'errors' => [
                    "name" => ['The name must not be greater than 255 characters.'],
                ],
            ]);
    }

    public function test_password_is_required()
    {

        $response = $this->postJson('auth/register', array_merge($this->request_data(), ['password' => '']));
        $response
            ->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJson([
                'message' => 'The password field is required.',
                'errors' => [
                    "password" => ['The password field is required.'],
                ],
            ]);
    }

    public function test_password_minimize()
    {

        $response = $this->postJson('auth/register', array_merge($this->request_data(), ['password' => '123']));
        $response
            ->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJson([
                'message' => 'The password must be at least 6 characters. (and 1 more error)',
                'errors' => [
                    "password" => [
                        "The password must be at least 6 characters.",
                        "The password confirmation does not match."
                    ],
                ],
            ]);
    }

    public function test_password_conformation_does_not_match()
    {
        $response = $this->postJson('auth/register', array_merge($this->request_data(), ['password_confirmation' => '']));
        $response
            ->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJson([
                'message' => 'The password confirmation does not match.',
                'errors' => [
                    "password" => ['The password confirmation does not match.'],
                ],
            ]);
    }

    public function test_email_is_valid()
    {
        $response = $this->postJson('auth/register', array_merge($this->request_data(), ['email' => 'email']));
        $response
            ->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJson([
                'message' => 'The email must be a valid email address.',
                'errors' => [
                    "email" => ['The email must be a valid email address.'],
                ],
            ]);
    }

    public function test_email_is_unique()
    {
        $user = User::factory()->create();
        $response = $this->postJson('auth/register', array_merge($this->request_data(), ['email' => $user->email]));
        $response
            ->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJson([
                'message' => 'The email has already been taken.',
                'errors' => [
                    "email" => ['The email has already been taken.'],
                ],
            ]);
    }


    public function test_register()
    {
        $response = $this->postJson('auth/register', $this->request_data());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson(['message' => 'Register successfully']);
        $this->assertDatabaseCount('users', 1);
    }
}
