<?php

namespace Modules\Auth\Tests\Feature;

use Modules\Core\Tests\Feature\ApiTestCase;
use Modules\User\Models\User;
use Modules\User\Transformers\StudentResource;
use Symfony\Component\HttpFoundation\Response;


class MeTest extends ApiTestCase
{

    public function test_me()
    {
        $user = User::factory()->create();
        $response = $this->actingAs($user)->getJson('auth/me');
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJsonFragment(
            [
                'id' => $user->id,
                'name' => $user->name,
                'email' => $user->email,
                'created_at' => $user->created_at,
                'updated_at' => $user->updated_at,
            ]
        );
    }

    public function test_me_if_not_registered()
    {
        $response = $this->getJson('auth/me', $this->request_data());
        $response->assertStatus(Response::HTTP_UNAUTHORIZED)
            ->assertJson(['message' => 'Unauthenticated.']);
    }

    private function request_data()
    {
        return [
            'email' => $this->faker()->email,
            'password' => $this->faker()->password,
        ];
    }

    private function dataResponse($user)
    {
        $userInformation = new StudentResource($user);
        return json_decode($userInformation->toJson(), true);
    }

}
