<?php

namespace Modules\Auth\Tests\Feature;

use Modules\Core\Tests\Feature\ApiTestCase;
use Modules\User\Models\User;
use Symfony\Component\HttpFoundation\Response;

class LogoutTest extends ApiTestCase
{

    public function test_logout_user()
    {
        $user = User::factory()->create();
        $user->createToken('auth_token')->plainTextToken;
        $response = $this->actingAs($user)
            ->getJson('auth/logout');
        $response
            ->assertStatus(Response::HTTP_OK)
            ->assertJson(['message' => 'Successfully logged out']);
    }

    public function test_logout_if_user_not_authenticated()
    {
        $response = $this->getJson('auth/logout');
        $response
            ->assertStatus(Response::HTTP_UNAUTHORIZED)
            ->assertJson(['message' => 'Unauthenticated.']);
    }

}
