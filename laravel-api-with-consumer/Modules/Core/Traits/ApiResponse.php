<?php

namespace Modules\Core\Traits;

use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Response;


trait ApiResponse
{

    protected function apiResponse($response)
    {
        if ($response['result']) {
            return $this->successResponse($response, $response['status_code']);
        } else {
            return $this->errorMessage($response['message'], $response['status_code']);
        }
    }

    protected function successResponse($data, $code = Response::HTTP_OK)
    {
        // when $data['data'] is collection and return response as json => response doesn't contain pagination
        if ($data['data'] instanceof AnonymousResourceCollection) {
            return $data['data'];
        } else {
            return response()->json($data, $code);
        }
    }

    protected function errorMessage($message, $code)
    {
        return response()->json(['message' => $message, 'status_code' => $code], $code);
    }

    protected function successMessage($message, $code = Response::HTTP_OK)
    {
        return $this->successResponse(['message' => $message, 'status_code' => $code], $code);
    }
}

