<?php

namespace Modules\Core\Traits;

trait ServiceResponse
{
    public function successResponse($data = null, $message = null, $code = 200)
    {
        return [
            'result' => true,
            'status_code' => $code,
            'message' => $message,
            'data' => $data
        ];
    }

    public function errorResponse($message, $code)
    {
        return [
            'result' => false,
            'status_code' => $code,
            'message' => $message,
        ];
    }
}

