<?php

namespace Modules\Core\Http\Controllers\API;

use Illuminate\Routing\Controller;
use Modules\Core\Traits\ApiResponse;

class ApiBaseController extends Controller
{
    use ApiResponse;
}
