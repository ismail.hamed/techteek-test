<?php

namespace Modules\User\Repositories;

use Modules\Core\Repositories\BaseRepository;
use Modules\User\Contracts\UserInterface;
use Modules\User\Models\User;

class UserRepository extends BaseRepository implements UserInterface
{

    public function __construct(User $model)
    {
        parent::__construct($model); // Inject the model that you need to build queries from
    }

}
