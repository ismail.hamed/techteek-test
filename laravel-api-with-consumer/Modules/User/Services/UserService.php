<?php

namespace Modules\User\Services;

use Modules\Core\Services\BaseService;
use Modules\User\Contracts\UserInterface;

class UserService extends BaseService
{
    protected $user;

    public function __construct(UserInterface $user)
    {
        $this->user = $user;
    }

}
