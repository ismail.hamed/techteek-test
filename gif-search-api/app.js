// Require Libraries
const express = require("express");
const cors = require("cors");
const http = require("http");
require("dotenv").config();

// App Setup
const app = express();
app.use(cors());
const port = process.env.PORT || 3000;
const Search_url = process.env.BASE_SEARCH_API;
const giphy_app_key = process.env.GIPHY_APP_KEY;

// Middleware

// Routes

app.get("/search/photos", (req, res) => {
  let queryStringTerm = req.query.query;
  let queryStringPerPage = req.query.per_page;
  let term = encodeURIComponent(queryStringTerm);
  let per_page = encodeURIComponent(queryStringPerPage);
  let url =
    Search_url +
    "search?q=" +
    term +
    "&api_key=" +
    giphy_app_key +
    "&limit=" +
    per_page;
  http.get(url, (response) => {
    response.setEncoding("utf8");
    let body = "";
    response.on("data", (d) => {
      body += d;
    });
    response.on("end", () => {
      let parsed = JSON.parse(body);
      let data = [];
      parsed.data.forEach((element) => {
        data.push({
          urls: {
            full: element.images.original.webp,
            thumb: element.images.preview_webp.url
              ? element.images.preview_webp.url
              : element.images.original.webp,
          },
          links: { download_location: element.url },
        });
      });
      let responseData = {
        results: data,
      };
      res.send(responseData);
    });
  });
});
// Start Server

app.listen(port, () => {
  console.log("Giphy Search listening on port: ", port);
});
